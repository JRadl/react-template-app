import React from 'react';
import './App.scss';
import {BrowserRouter} from 'react-router-dom'
import { Provider } from 'react-redux';
import ScrollToTop from 'react-router-scroll-top';
import{store, persistor} from './resources/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import RootComponent from './Root';
import { isUserLogged, getCurrentUserData } from './resources/api/user';
import { loginSuccess } from './resources/redux/actions';


export default function App(){
    getCurrentUserData().then(res => {
        if(res){
            store.dispatch(
                loginSuccess(res)
            )
        }
    });
    
    return <Provider store={store}>                             {/*Redux provieder */}
        {/*<PersistGate loading={null} persistor={persistor}>      Redux persistor */}
            <BrowserRouter>                                     {/*React router */}
                <ScrollToTop>                                   {/*Scroll to top after react router redirect */}
                    <RootComponent/>                            {/*Application root component */}
                </ScrollToTop>
            </BrowserRouter>
        {/*</PersistGate>*/}
    </Provider>
}