import React from 'react';
import  {Switch, Route, Redirect} from 'react-router'
import { connect } from 'react-redux';

import Toolbar from './components/toolbar/Toolbar';
import Footer from './components/footer/Footer';

import Error404 from './pages/errors/Error404';

import LandingPage from './pages/LandingPage';
import AboutPage from './pages/AboutPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import TestPage from './pages/TestPage';

import ContentsIndex from './pages/contents/ContentsIndex';
import UserIndex from './pages/user/UserIndex';

const RootComponent = ({logged}) => <div id="app">
    <Toolbar/>
    <Switch>
        <Route exact path="/"><LandingPage/></Route>
        <Route path="/home"><LandingPage/></Route>
        <Route path="/contents"><ContentsIndex/></Route>
        <Route path="/about"><AboutPage/></Route>

        <Route path="/login">{!logged?<LoginPage/>:<Redirect to="/user"/>}</Route> 
        <Route path="/register">{!logged?<RegisterPage/>:<Redirect to="/user"/>}</Route>
        
        <Route path="/user"><UserIndex/></Route>
        <Route path="/test"><TestPage/></Route>
        <Route path=""><Error404/></Route>
    </Switch>
    <Footer/>
</div>

const mapStateToProps = state => ({
    logged: state.login.logged
})

export default connect(mapStateToProps, ()=>{})(RootComponent)