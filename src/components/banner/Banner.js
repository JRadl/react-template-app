import React from 'react';
import './Banner.scss';

/**
 * Message banner
 * 
 * @param {string} type banner type -- options: ["success", "error", "neutral", "loading"]
 * @param {string} message banner message
 */
export default function Banner({type, message}){
    return <div className={"banner "+type}>{message}</div>
}