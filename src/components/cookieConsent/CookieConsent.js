import React from 'react';
import './CookieConsent.scss';
import Cookie from 'js-cookie';
import Button from '../button/Button';


export default class CookieConsent extends React.Component{
    constructor(){
        super()
        this.state = {closed: false}
    }

    render(){
        if (Cookie.get("consent")) return <></>
        return <div id="cookieConsent" className={(this.state.closed?" closed":"")} onClick={() => {this.setState({closed: true}, () => {
            Cookie.set("consent","true", {expires: 365}); 
            new Promise(resolve => setTimeout(resolve, 700)).then(() => this.forceUpdate());
        })}} >
            Tento web využívá pro svou funkci soubory cookies. Návštěvou tohoto webu s jejich použitím souhlasíte. <Button variant="full">Souhlasím</Button>
        </div>
    }
}