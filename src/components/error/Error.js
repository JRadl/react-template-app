import React from 'react';

/**
 * Returns text with displayed error code and provided message
 * 
 * @param {int} num HTML Error code number 
 * @param {string} text Error type description 
 * @param {string} center opt What gets displayed in the centre of the screen
 */

export default function Error({num, text, center}){
    return <section>
            <main>
                <h1><span className="accent">{num}</span> &ndash; {text}</h1>
                <center>{center}</center>
            </main>
    </section>
}