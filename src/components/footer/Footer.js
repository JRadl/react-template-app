import React from 'react';
import './Footer.scss';
import Logo from '../logo/Logo'
import {Link} from 'react-router-dom'

/**
 * Footer component
 */
export default function Footer(){
    return <div id="footer">
        <main>
            <div className="grid">
                <div>
                    <Logo/>
                    <div className="copyright">&copy; Template &ndash; všechna práva vyhrazena</div>
                </div>
                <div className="sites">
                    <div className="title">Stránky</div>
                    <Link to="/home"><div className="link">Home</div></Link>
                    <Link to="/contents"><div className="link">Contents</div></Link>
                    <Link to="/about"><div className="link">About</div></Link>
                </div>
                <div className="contact">
                    <div className="title">Kontakt</div>
                    <div className="emailLabel">E-mail</div>
                    <a href="mailto://mail@example.org"><div className="email">mail@example.org</div></a>
                </div>
            </div>
        </main>
    </div>
}