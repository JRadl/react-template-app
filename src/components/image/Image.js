import React from 'react';


/**
 * Image component
 * @param {string} imgSrc Image url
 * @param {number} ratio Image div width:height ratio -- ex. {16/9}
 * @param {string} variant background size -- options ["contain", "cover"] default: "cover"
 * all props are passed to child
 */
export default function Image(props){
    return <div className="image" {...props}>
        <div className="imageInner" style={{
            backgroundImage: "url("+props.imgSrc+")",
            paddingTop: (100/props.ratio)+"%",
            backgroundSize: props.variant=="contain"?"contain":"cover",
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            width: "100%",
        } }>

        </div>
    </div>
}

/**
 * Image component with asynchronous url
 * 
 * @param {function} asyncSrc
 * all props are passed to child
 */
export class AsyncImage extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            src: "",
            bg:"#FFF"
        }
        this.load()
    }

    async load(){ 
        try{
            this.setState({src: await this.props.asyncSrc()})
        }catch(e){
            console.log(e);
        }
        
    }

    componentDidUpdate(prevProps){
        if(this.props.asyncSrc != prevProps.asyncSrc)this.load()
    }

    render(){
        return <Image imgSrc={this.state.src} {...this.props}/>
    }
}