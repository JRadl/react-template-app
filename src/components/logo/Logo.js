import React from 'react';
import './Logo.scss';

/**
 * Logo component
 */
export default function Logo(){
    return <div className="logoComponent"><span class="accent">LO</span>GO</div>
}