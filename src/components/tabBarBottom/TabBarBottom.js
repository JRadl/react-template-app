import React from 'react';
import { Link } from 'react-router-dom';
import './TabBarBottom.scss';

export class TabBarBottom extends React.Component{
    constructor(props){
        super(props)
        
        this.state = {
            lastOffset: 0,
            offset:0,
            class: "down",
        }

        window.addEventListener('scroll', (event) => {
            this.setState({lastOffset:this.state.offset});
            this.setState({offset:window.pageYOffset});

            if(this.state.offset > this.state.lastOffset)this.setState({class:"up"});
            if(this.state.offset < this.state.lastOffset)this.setState({class:"down"});
        })
    }

    render(){
        return <div className={"tabBarBottom "+ this.state.class}>
        <div className="title">
            <div>{this.props.title}</div>
        </div>
        <div className="content">
            {this.props.children}
        </div>
    </div>
    }
}


export function TabBottom({path, to, text, icon}){
    return <Link to={to}>
        <div className={" tab "+(doesPathMatch(path)?" active ":"")}>
            {icon}
            <div>{text}</div>    
        </div>
    </Link>
}

function doesPathMatch(path){
    const win = window.location.pathname.split("/")
    path = path.split("/")
    var match = true;

    for(var key in win){
        if(win[key][0] !== ":" && path[key][0] !== ":"){
            if(win[key]!==path[key]) match = false;
        }
    }

    for(var key in path){
        if(path[key][0] !== ":" && path[key][0] !== ":"){
            if(win[key]!==path[key]) match = false;
        }
    }

    return match
}