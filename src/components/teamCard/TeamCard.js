import React from 'react';
import './TeamCard.scss';
import { FaEnvelope, FaFacebookF, FaFacebookMessenger, FaAt } from 'react-icons/fa'

/**
 * Returns card of a person
 * 
 * @param {string} name Name of person
 * @param {string} imgSrc Path to photo
 * @param {string} bullets array Info about person
 * @param {string} mail Email adress
 * @param {string} facebook Facebook contact link
 * @param {string} messenger Messenger contact link
 */

export default function TeamCard({name, imgSrc, bullets = [], mail, facebook, messenger}){
    return <div className="teamCard">
        <div className="profilePictureWrapper"><div className="profilePicture" style={{backgroundImage:"url("+imgSrc+")"}}></div></div>
        <div className="name">{name}</div>
        <div className="bio">
            <ul>
                {bullets.map(i => <li><span>{i}</span></li>)}
            </ul>
        </div>
        <div className="socials">
            <a href={"mailto://"+mail}><div className="site"><FaEnvelope/></div></a>
            <a href={facebook}><div className="site"><FaFacebookF/></div></a>
            <a href={messenger}><div className="site"><FaFacebookMessenger/></div></a>
        </div>
    </div>
}