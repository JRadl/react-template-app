import React from 'react';
import './Toolbar.scss';
import { FaBars, FaSignOutAlt } from 'react-icons/fa';
import Logo from '../logo/Logo';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { logOut } from '../../resources/redux/actions';
import Image from '../image/Image'
import Button from '../button/Button';

/**
 * Toolbar Component
 */
class Toolbar extends React.Component{
    constructor(){
        super();
        this.state = {
            lastOffset: 0,
            offset:0,
            class: "top",
            menuOpen: false,
        }

        window.addEventListener('scroll', (event) => {
            this.setState({lastOffset:this.state.offset});
            this.setState({offset:window.pageYOffset});

            if(this.state.offset > this.state.lastOffset)this.setState({class:"hidden", menuOpen:false});
            if(this.state.offset < this.state.lastOffset)this.setState({class:"show"});
            //if(this.state.offset < 80)this.setState({class:"top"});
        })
    }

    render(){
        return <div id="toolbar" className={this.state.class + " " + (this.state.menuOpen&&"menu")}>
            <div id="content">
                <Link to="/home" onClick={() => this.setState({menuOpen:false})}><Logo/></Link>
                <div className="filler"></div>
                <div className="sites">
                    <Link to="/contents"><div className="link">Contents</div></Link>
                    <Link to="/about"><div className="link">About</div></Link>
                </div>
                {!this.props.logged&&
                <div className="buttons">
                    <Link to="/login" onClick={() => this.setState({menuOpen:false})}><Button >Přihlásit se</Button></Link>
                    <Link to="/register" onClick={() => this.setState({menuOpen:false})}><Button variant="full">Registrovat</Button></Link>
                </div>
                }
                {this.props.logged&&<>
                    <Link to="/user" onClick={() => this.setState({menuOpen:false})}><div class="userButton"><Image imgSrc={this.props.picture} ratio={1}/></div></Link>
                    <div class="logoutButton" onClick={() => {this.props.logout(); this.setState({menuOpen:false})}}><FaSignOutAlt/></div>
                </>}
                <div id="menuButton" onClick={() => this.setState({menuOpen: !this.state.menuOpen})}><FaBars/></div>
            </div>
            
            <div id="dropDown" className={this.state.menuOpen&&"open"}>
                <div className="sites">
                    <Link to="/contents"><div onClick={() => this.setState({menuOpen:false})} className="item">Contents</div></Link>
                    <Link to="/about"><div onClick={() => this.setState({menuOpen:false})} className="item">About</div></Link>
                </div>
                <div className="buttons">
                    <Link to="/login"><div onClick={() => this.setState({menuOpen:false})} className="item">Přihlásit se</div></Link>
                    <Link to="/register"><div onClick={() => this.setState({menuOpen:false})} className="item">Registrovat</div></Link>
                </div>
            </div>
        </div>
    }

}

const mapStateToProps = state => ({
    logged: state.login.logged,
    picture: state.user.profilePhoto,
})

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logOut())
})

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar)