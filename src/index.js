import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/**
 * Renders App.js to website
 */

ReactDOM.render(<App/>,document.getElementById('root'));
