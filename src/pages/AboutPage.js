import React from 'react';
import './AboutPage.scss';
import Input from '../components/input/Input';
import Textarea from '../components/textarea/Textarea';
import TeamCard from '../components/teamCard/TeamCard';
import Button from '../components/button/Button';
import Banner from '../components/banner/Banner';

/**
 * Page about developer team as well as about R-NOC
 */

export default function AboutPage(){
    return <div className="page" id="aboutPage">
        <section>
            <main>
                <h1>O nás</h1>
                <div className="description">
                    Ullamco velit aliqua Lorem laborum anim pariatur ut sunt tempor magna non. Exercitation Lorem eiusmod quis ex nostrud Lorem sit ipsum. Aute do elit incididunt labore quis voluptate fugiat minim irure ipsum. Laboris ea ea dolore voluptate eu ex laborum.
                    Voluptate ea ea velit cupidatat mollit nisi cupidatat enim id. Proident magna aliqua excepteur deserunt aliquip labore sunt enim nisi exercitation incididunt. Est in occaecat magna ea veniam veniam laborum esse. Dolore quis id anim minim sit minim do non.
                </div>
                <div id="team">
                    <TeamCard 
                        name="Lorem Ipsum" 
                        imgSrc="https://api.adorable.io/avatars/500/93198.png" 
                        bullets={["Dolor sit amet","Lorem lorem ipsum ispum"]}
                        mail="mail@example.org"
                        facebook=""
                        messenger=""
                    />
                    <TeamCard 
                        name="Lorem Ipsum" 
                        imgSrc="https://api.adorable.io/avatars/500/3DJLKNK.png" 
                        bullets={["Dolor sit amet","Lorem lorem ipsum ispum"]}
                        mail="mail@example.org"
                        facebook=""
                        messenger=""
                    />
                    <TeamCard 
                        name="Lorem Ipsum" 
                        imgSrc="https://api.adorable.io/avatars/500/SJD08.png" 
                        bullets={["Dolor sit amet","Lorem lorem ipsum ispum"]}
                        mail="mail@example.org"
                        facebook=""
                        messenger=""
                    />
                    <TeamCard 
                        name="Lorem Ipsum" 
                        imgSrc="https://api.adorable.io/avatars/500/1234.png" 
                        bullets={["Dolor sit amet","Lorem lorem ipsum ispum"]}
                        mail="mail@example.org"
                        facebook=""
                        messenger=""
                    />
                </div>
            </main>
        </section>
        <section id="contactSection">
            <main>
                <h2>Kontakt</h2>
                Pokud nám chceš něco sdělit, pochválit web, či nahlásit chybu, můžeš nám buď napsat na email <accent><a href="mailto://mail@example.org">mail@example.org</a></accent>, napsat někomu z nás pomocí některého z kontaktů výše, nebo využít formulář pro rychlý feedback.
                <FeedbackForm/>
            </main>
        </section>
    </div>
}


class FeedbackForm extends React.Component{
    constructor(){
        super()
        this.state={
            name: "",
            email: "",
            text: "",
            status: "NONE",
            working: false,
        }
    }

    submit(){
        this.setState({validate: true})
        var form = document.getElementById("contactForm")
        if(!form.checkValidity()){
            var tmpSubmit = document.createElement('button')
            form.appendChild(tmpSubmit)
            tmpSubmit.click()
            form.removeChild(tmpSubmit)
            return
        }
        if(!(this.state.name&&this.state.email&&this.state.text&&!this.state.working))return;
        this.setState({working:true, status:"NONE"})
        const xhr = new XMLHttpRequest();
        const data = new FormData();
        data.append("name",this.state.name)
        data.append("email",this.state.email)
        data.append("message",this.state.text)
        //xhr.open("POST","https://formspree.io/xgelbqag")
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== XMLHttpRequest.DONE) return;
            if (xhr.status == 0){
                this.setState({status:"SUCCESS"})
                this.setState({
                    name: "",
                    email: "",
                    text: "",
                    working:false,
                    validate: false,
                })
            } 
            else{
                this.setState({status:"ERROR", working:false})
            }
        }
        xhr.send(data);
        
    }

    render(){
        return <form className={this.state.validate&&"validate"} id="contactForm">
            {this.state.working&&<Banner type="loading" message="Odesílá se ..."/>}
            {this.state.status=="ERROR"&&<Banner type="error" message="Tvou zprávu se bohužel nepodařilo odeslat."/>}
            {this.state.status=="SUCCESS"&&<Banner type="success" message="Tvůj feedback byl úspěšně odeslán."/>}
            <div className="grid">
                <Input 
                    type="text" 
                    placeholder="Lorem Ipsum" 
                    label="Jméno" 
                    onChange={(e) => {this.setState({name: e.target.value})}} 
                    value={this.state.name}
                    required
                />
                <Input 
                    type="email" 
                    placeholder="dolor@sitamet.com" 
                    label="E-mail" 
                    onChange={(e) => {this.setState({email: e.target.value})}} 
                    value={this.state.email} 
                    required
                />
            </div>
            <Textarea 
                placeholder="Co nám chceš říct?" 
                label="Text" 
                onChange={(e) => {this.setState({text: e.target.value})}} 
                value={this.state.text}
                required
            />
            <div className="button">
                
                <Button variant="full" onClick={() => this.submit()}>Odeslat</Button>
            </div>
            
        </form>
    }
}