import React from 'react';
import HeaderSection from './landingPage/HeaderSection';
import SecondSection from './landingPage/SecondSection';
import ThirdSection from './landingPage/ThirdSection';
/**
 * Landing page from it's 3 parts
 */
export default function LandingPage(){
    return <div className="page">
        <HeaderSection/>
        <SecondSection/>
        <ThirdSection/>
    </div>
}