import React from 'react'
import Banner from '../components/banner/Banner'
import { login } from '../resources/redux/actions'
import { connect } from 'react-redux'
import {Redirect} from 'react-router-dom'
import Form from '../components/form/Form'
import './LoginPage.scss';

class LoginPage extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            redirect: false,
        }
    }

    componentDidUpdate(){
        if(this.props.success)setTimeout(()=>{this.setState({redirect:true})}, 1000)
    }

    render(){
        if (this.state.redirect) return <Redirect to="/user"/>
        return <div className="page" id="loginPage">
            <main>
                <h1>Login</h1>
                {this.props.loggingIn&&<Banner type="loading" message="Přihlašuji ..."/>}
                {this.props.error&&<Banner type="error" message={this.props.error}/>}
                {this.props.success&&<Banner type="success" message="Úspěšně přihlášen"/>}
                <Form
                    inputs={[
                        {type: "email", name:"email", placeholder:"email@example.org", label:"E-mail", email: true, required: true},
                        {type: "password", name:"password", placeholder:"heslo123", label:"Heslo", required: true}
                    ]}
                    button="Log In"
                    submit={values => this.props.login(values.email, values.password)}
                />
            </main>
        </div>
        
    }
}


const mapDispatchToProps = dispatch => ({
    login: (email, password) => {
        dispatch(login(email, password))
    }
})

const mapStateToProps = state => ({
    loggingIn: state.login.working,
    success: state.login.logged,
    error: state.login.error,
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
