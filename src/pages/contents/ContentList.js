import React from 'react';
import './ContentList.scss';
import {getContents} from '../../resources/api/content';
import { Link } from 'react-router-dom';
/**
 * Displays Planned, current and past contents
 */
export default class ContentList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            contents: {}
        }
    }

    componentDidMount(){
        getContents().then(res => this.setState({contents: res}))
    }

    list(){
        var list = [];
        for(var key in this.state.contents){
            list.push(<Link to={"/contents/"+key}><div className="item">{JSON.stringify(this.state.contents[key])}</div></Link>)
        }
        return list;
    }

    render(){
        return <div className="page" id="contentsPage">
            <main>
                <h1>Akce</h1>
                <div className="contents">
                    {this.list()}
                </div>
            </main>
        </div>
    }
}