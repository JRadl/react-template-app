import React from 'react';
import { Switch, Route } from 'react-router';
import ContentList from './ContentList';
import ContentIndex from './content/ContentIndex';


/**
 * Index of the "/contents" page
 */
export default function ContentsIndex(){
    return <Switch>
        <Route path="/contents/:id" render={(context) => <ContentIndex context={context}/>}/>
        <Route path="/contents/"><ContentList/></Route>
        
    </Switch>
    
}