import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { FaInfo, FaChalkboardTeacher, FaClipboardList, FaRegSmileWink, FaRegSurprise, FaRegMeh } from 'react-icons/fa';
import Banner from '../../../components/banner/Banner';
import { TabBarTop, TabTop } from '../../../components/tabBarTop/TabBarTop';
import { TabBarBottom, TabBottom } from '../../../components/tabBarBottom/TabBarBottom';
import FirstTab from './FirstTab';
import ThirdTab from './ThirdTab';
import SecondTab from './SecondTab';
import { getContent } from '../../../resources/api/content';

/**
 * Index of Content detail page "/contents/:id"
 * @param {*} context Router context
 */
export default class ContentIndex extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            content: {},
            error: ""
        }
    }

    componentDidMount(){
        getContent(this.props.context.match.params.id).then(res => {
            this.setState({content: res})
        }).catch(({message}) => {
            this.setState({error: message})
        })
    }

    render(){
        return <div className="page withBottomBar">
            <TabBarTop>
                <TabTop 
                    text="First" 
                    icon={<FaRegSmileWink/>} 
                    to={"/contents/"+this.props.context.match.params.id+"/first"} 
                    path="/contents/:id/first"
                />
                <TabTop 
                    text="Second" 
                    icon={<FaRegSurprise/>} 
                    to={"/contents/"+this.props.context.match.params.id+"/second"} 
                    path="/contents/:id/second"
                />
                <TabTop
                    text="Third" 
                    icon={<FaRegMeh/>} 
                    to={"/contents/"+this.props.context.match.params.id+"/third"} 
                    path="/contents/:id/third"
                />
            </TabBarTop>
            {!this.state.error && 
            <Switch>
                <Route path="/contents/:id/first"><FirstTab {...this.state.content}/></Route>
                <Route path="/contents/:id/second"><SecondTab/></Route>
                <Route path="/contents/:id/third"><ThirdTab/></Route>
                <Route path="/contents/:id/" exact><Redirect to={"/contents/"+this.props.context.match.params.id+"/first"}/></Route>
            </Switch>}
            {this.state.error && <main><Banner type="error" message={this.state.error}/></main>}
            <TabBarBottom>
                <TabBottom
                    text="First" 
                    icon={<FaRegSmileWink/>} 
                    to={"/contents/"+this.props.context.match.params.id+"/first"} 
                    path="/contents/:id/first"
                />
                <TabBottom
                    text="Second" 
                    icon={<FaRegSurprise/>} 
                    to={"/contents/"+this.props.context.match.params.id+"/second"} 
                    path="/contents/:id/second"
                />
                
                <TabBottom
                    text="Third" 
                    icon={<FaRegMeh/>} 
                    to={"/contents/"+this.props.context.match.params.id+"/third"} 
                    path="/contents/:id/third"
                />
            </TabBarBottom>
        </div>
    }

}