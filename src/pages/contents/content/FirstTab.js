import React from 'react';

export default function ContentFirstTab(props){
    return <div className="tab">
        <main>
            <h2>{props.title}</h2>
            <p>
                Non sint duis consequat dolor laborum incididunt fugiat. Occaecat reprehenderit magna velit consequat exercitation cillum Lorem mollit qui ea aliquip ex tempor Lorem. Aliqua reprehenderit quis dolore nisi qui qui laborum nisi irure exercitation qui quis deserunt. Amet deserunt quis magna commodo nostrud et ea excepteur ad ipsum exercitation consequat nulla. Magna sint exercitation cillum adipisicing ea mollit veniam dolore cupidatat sit sint nostrud eu veniam. Nostrud aute nisi aliquip aliquip consequat dolore tempor laborum in nisi culpa occaecat dolore. Lorem reprehenderit nostrud adipisicing exercitation deserunt non pariatur pariatur.
            </p>
            <p>
                Magna laboris exercitation eiusmod mollit sit laborum nostrud pariatur adipisicing aliquip do. Voluptate deserunt dolor esse sint nisi quis. Occaecat sunt eu consequat non cillum laboris labore id pariatur id labore. Lorem commodo deserunt nulla esse. Tempor et aliquip duis consequat excepteur culpa pariatur cillum enim est incididunt anim aute in.
            </p>
        </main>
    </div>
}