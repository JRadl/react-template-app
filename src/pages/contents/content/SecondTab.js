import React from 'react';

export default function ContentSecondTab({title}){
    return <div className="tab">
        <main>
            <h2>Second tab</h2>
            <p>Aliqua irure culpa enim elit non adipisicing nostrud. Eiusmod esse reprehenderit aliqua ullamco ea sint sunt culpa. Velit reprehenderit cupidatat culpa magna nisi aliquip excepteur. Pariatur irure aute mollit mollit deserunt reprehenderit. Enim mollit exercitation amet adipisicing eiusmod non nulla est tempor sint. Est do consequat duis duis.</p>

            <p>Aliqua pariatur ad sint labore dolore sint aliquip exercitation fugiat et eiusmod magna amet. Deserunt labore sit dolor dolore aliqua laborum consectetur irure proident quis enim ut reprehenderit. Ut pariatur aute tempor veniam veniam ullamco adipisicing. Anim quis sit ad dolore et officia reprehenderit deserunt. Labore reprehenderit dolore est sunt laboris elit in id. Sit irure nulla commodo ut aute incididunt culpa pariatur eiusmod proident sunt pariatur. Adipisicing sit dolor cillum sit voluptate.</p>

            <p>Laboris sit ut reprehenderit nisi ullamco. Lorem laborum est laboris irure dolore adipisicing nisi nulla minim. Deserunt consectetur reprehenderit nisi ex id qui proident non est in aliquip. Quis commodo enim deserunt voluptate consequat est amet commodo dolore voluptate labore excepteur. Proident irure excepteur excepteur pariatur nostrud sint occaecat velit esse cupidatat anim qui. Elit proident excepteur commodo aute amet sit ex aute irure esse ex reprehenderit exercitation est. Magna cillum labore minim ullamco.</p>

            <p>Mollit qui incididunt quis laborum magna irure exercitation cillum tempor do est qui. Veniam mollit irure deserunt occaecat adipisicing eu tempor. Mollit sint ad laborum reprehenderit exercitation labore commodo tempor velit incididunt qui Lorem.</p>
        </main>
    </div>
}