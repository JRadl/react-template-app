import React from 'react';

export default function ContentThirdTab(props){
    return <div className="tab">
        <main>
            <h2>Third tab</h2>
            <p>
                Cupidatat ipsum anim dolore nulla incididunt enim ex. Dolore magna velit qui aute. Et reprehenderit cupidatat sint voluptate irure nulla est excepteur nulla sunt nostrud reprehenderit sint. Culpa nostrud dolor fugiat veniam ea officia. Qui ad labore officia velit occaecat amet velit nisi enim.
            </p>
        </main>
    </div>
}