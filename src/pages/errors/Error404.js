import React from 'react';
import Logo from '../../components/logo/Logo';
import { GiTeacher } from 'react-icons/gi';
import './Error404.scss'

export default function Error404(){
    return <div className="page" id="Error404">
            <main>
                <h1><span className="accent">404</span> &ndash; Stránka nenalezena</h1>
                <center><h1 className="shrug">¯\_(ツ)_/¯</h1></center>
            </main>
    </div>
}