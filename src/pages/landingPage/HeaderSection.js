import React from 'react';
import './HeaderSection.scss';
import { FiChevronDown } from 'react-icons/fi';


export default function HeaderSection(){
    return <section id="headerSection">
        <main>
            <h1>Template Application</h1>
            <p>
                Irure laborum cupidatat voluptate veniam eu in voluptate consequat deserunt. Esse ex nisi quis ullamco id magna ipsum irure ad. Sint cillum mollit laborum ea eiusmod dolore labore. Enim enim veniam Lorem dolore dolore pariatur minim fugiat. Adipisicing reprehenderit amet pariatur ex est enim ullamco proident in commodo fugiat velit magna occaecat. Laboris officia eu aliquip sint elit aliqua anim.In sint minim reprehenderit nulla et. Id amet in incididunt deserunt excepteur aliqua magna id aliquip proident. Esse adipisicing ea aliqua consequat non ullamco ullamco cillum sint sint dolor ad. Ullamco sunt enim consectetur eu sint nostrud nisi amet qui. Commodo elit nisi exercitation ex labore elit voluptate nulla excepteur nostrud do proident. Aute sunt cillum sint tempor officia nostrud sint voluptate ea sunt aliquip id deserunt. Amet consectetur fugiat elit et incididunt irure quis proident excepteur elit officia duis.
            </p>
            <p>
                Quis fugiat do fugiat magna dolore voluptate anim ullamco deserunt id eu. Eu officia deserunt consectetur et et ad. Duis velit eu magna nisi consectetur. Laborum amet veniam exercitation incididunt. Velit mollit proident pariatur aliquip. Ex sunt pariatur proident eiusmod elit velit deserunt mollit non eu reprehenderit sunt nostrud.Aliqua deserunt tempor do amet elit ea. Labore culpa deserunt labore irure laboris aute quis commodo nisi. Ex cillum occaecat Lorem quis aute incididunt veniam.
            </p>
            
        </main>
        <div id="scrollDown">
            <a href="#secondSection"><FiChevronDown/></a>
        </div>
    </section>
}