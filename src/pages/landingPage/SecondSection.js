import React from 'react';
import './SecondSection.scss';
/**
 * 2nd section of Landing page
 */
export default function SecondSection(){
    return <section id="secondSection">
        <main>
            <h2 className="center">Second Section</h2>
            <p>
                Laborum esse deserunt nostrud cupidatat laboris ea reprehenderit do do reprehenderit. Excepteur dolor ad cillum labore laboris nostrud est minim. Minim veniam sint proident irure consequat amet ipsum consequat officia eiusmod mollit. Excepteur id irure dolore consectetur qui consectetur ad commodo voluptate elit pariatur.
            </p>
        </main>
    </section>
}
