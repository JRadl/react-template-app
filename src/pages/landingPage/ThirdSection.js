import React from 'react';
import './ThirdSection.scss';
import Button from '../../components/button/Button'
/**
 * 3rd section of Landing Page
 */
export default function ThirdSection(){
    return <section id="thirdSection">
        <main>
            <h2 className="center">Third Section</h2>
            <p className="center">
                Reprehenderit proident mollit occaecat cillum eu esse duis. Dolor enim sint minim ipsum labore dolor culpa veniam aliquip. Consectetur in amet dolore esse qui esse sunt ex commodo esse sint Lorem. Magna non dolor aliqua reprehenderit Lorem occaecat incididunt nostrud voluptate pariatur voluptate elit.
            </p>
            <div className="button">
                <Button>Some button</Button>
            </div>
        </main>
    </section>
}