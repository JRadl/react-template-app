import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import UserPrivateIndex from './private/UserPrivateIndex';
import { connect } from 'react-redux';
import { UserPublicIndex } from './public/UserPublicIndex';


/**
 * Index of the "/user" page
 */

function UserIndex(){
    return <Switch>
        <Route path="/user/me" render={(context) => <UserPrivateIndex context={context}/>}/>
        <Route path="/user/:id" render={(context) => <UserPublicIndex context={context}/>}/>
        <Route path="/user"><Redirect to={"/user/me"}/></Route>
    </Switch>  
}

export default UserIndex