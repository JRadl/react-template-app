import React from 'react';
import { changePicture } from '../../../resources/redux/actions';
import { connect } from 'react-redux';
import Banner from '../../../components/banner/Banner'

class UserProfileTab extends React.Component{
    constructor(props){
        super(props)
        this.state = {
        }
    }

    render(){
        return <div className="tab">
            <main>
                {this.props.saving&&<Banner type="loading" message="Ukládám data ..."/>}
                {this.props.saved&&<Banner type="success" message="Data uložena."/>}
                {this.props.error&&<Banner type="error" message={this.props.error}/>}
                <h2>Profil</h2>
            </main>
        </div>
    }
}

const mapStateToProps = state => ({
    user: state.user,
    saving: state.user.savingData,
    error: state.user.error,
    saved: state.user.dataSaved,
})

const mapDispatchToProps = dispatch => ({
    changePicture: string => {
        dispatch(changePicture(string))
    }
})



export default connect(mapStateToProps, mapDispatchToProps)(UserProfileTab)
