import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import UserProfileTab from './ProfileTab';
import UserSettingsTab from './SettingsTab';
import Banner from '../../../components/banner/Banner';
import { Link } from 'react-router-dom';
import Button from '../../../components/button/Button';
import { FaCog, FaChalkboardTeacher, FaUser, FaPlaceOfWorship, FaCcApplePay, FaBellSlash } from 'react-icons/fa';
import { TabTop, TabBarTop } from '../../../components/tabBarTop/TabBarTop';
import { TabBarBottom, TabBottom } from '../../../components/tabBarBottom/TabBarBottom';
import { connect } from 'react-redux';
import UserSomethingTab from './SomethingTab';


/**
 * Index of the "/user" page
 */
class UserPrivateIndex extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            
        }
    }
    
    render(){    
    //if(this.props.redirect) return <Redirect to="/home"/>
    return  <div className="page withBottomBar">
        <TabBarTop title={"User"}>
            <TabTop
                text="Profil" 
                icon={<FaUser/>} 
                to={"/user/me/profile"} 
                path="/user/me/profile"
            />
            <TabTop 
                text="Something" 
                icon={<FaBellSlash/>} 
                to={"/user/me/something"} 
                path="/user/me/something"
            />
            <TabTop 
                text="Nastavení" 
                icon={<FaCog/>} 
                to={"/user/me/settings"} 
                path="/user/me/settings"
            />
        </TabBarTop>
        <Switch>
            <Route path="/user/me/profile"><UserProfileTab/></Route>
            <Route path="/user/me/something"><UserSomethingTab/></Route>
            <Route path="/user/me/settings"><UserSettingsTab/></Route>
            <Route path="/user/me/"><Redirect to={"/user/me/profile"} /></Route>
        </Switch>
        <TabBarBottom title={"User"}>
            <TabBottom
                text="Profil" 
                icon={<FaUser/>} 
                to={"/user/me/profile"} 
                path="/user/me/profile"
            />
            <TabBottom
                text="Přednášky" 
                icon={<FaBellSlash/>} 
                to={"/user/me/something"} 
                path="/user/me/something"
            />
            <TabBottom
                text="Nastavení" 
                icon={<FaCog/>} 
                to={"/user/me/settings"} 
                path="/user/me/settings"
            />
        </TabBarBottom>
    </div>
    }
}

export default connect(state => ({redirect: !state.login.logged}))(UserPrivateIndex)
