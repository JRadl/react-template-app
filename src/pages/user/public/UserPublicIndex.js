import React from 'react';

export function UserPublicIndex(props){
    return <div className="page">
        <section>
            <main>
                <h1>User Public Index {props.context.match.params.id}</h1>
            </main>
        </section>
    </div>
}