/**
 * This is library thet requests data about Contents from the 
 * server, parses them and returns them to the components
 */

const contents = require('../placeholderData/contents.json');

export async function getContent(id){
    if (Object.keys(contents).includes(id.toString())){
        return contents[id.toString()]
    }
    throw new Error("Content not found!")
}

export async function getContents(){
    return contents
}