/**
 * This is a library for user authentication and management
 */
import Cookie from 'js-cookie';
const users = require('../placeholderData/users.json');

/**
 * Checks user info with database and returns user object
 * @param {string} email 
 * @param {string} password 
 * @throws "Wrong password!" if password does not match
 * @throws "Wrong email!" if email is not in database
 */
export async function userLogin(email, password){
    await new Promise(resolve => setTimeout(resolve, 1500));
    for(var key in users){
        if(email === users[key].email){
            if(password === users[key].password){
                Cookie.set("userId",key);
                return users[key]
            }
            throw(new Error("Wrong password!"))
        }
    }
    throw(new Error("Wrong email!"))

}

/**
 * 
 */
export async function userChangePicture(){
    await new Promise(resolve => setTimeout(resolve, 1500));
    if(Math.random()>0.7)throw(new Error("Random error thrown"))
    return "Obrázek úspěšně uložen";
}

export async function userLogout(){
    Cookie.remove("userId");
    return true;
}

export async function getCurrentUserData(){
    const id = Cookie.get("userId");
    if(id){
        return users[id]
    }
    return false
}