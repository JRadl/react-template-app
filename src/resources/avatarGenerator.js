import mergeImages from 'merge-images';


export const eyes = ["eyes1","eyes10","eyes2","eyes3","eyes4","eyes5","eyes6","eyes7","eyes9","eyes11","eyes13","eyes14","eyes15","close","open"];
export const mouths = ["mouth1","mouth10","mouth11","mouth3","mouth5","mouth6","mouth7","mouth9","mouth13"];
export const noses = ["nose2","nose3","nose4","nose5","nose6","nose7","nose8","nose9","plnos"];

export async function getImage(eyes, nose, mouth, color){   
    const background = '<svg xmlns="http://www.w3.org/2000/svg" height="400" width="400"><rect height="400" width="400" style="fill: #'+color+';"/></svg>';
    const url = "data:image/svg+xml;base64,"+window.btoa(background)
    return await mergeImages([url, "/img/avatars/eyes/"+eyes+".png","/img/avatars/mouth/"+mouth+".png", "/img/avatars/nose/"+nose+".png"])
}

export function parse(xxx){
    var arr = xxx.split("/");
    return {
        eyes: arr[0],
        nose: arr[1],
        mouth: arr[2],
        color: arr[3],
    }
}
