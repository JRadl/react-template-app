import { userLogin, userChangePicture, userLogout } from '../api/user'

export const CHANGE_PICTURE = 'CHANGE_PICTURE';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const START_LOGIN = 'START_LOGIN'
export const LOGIN_FAILED = 'LOGIN_FAILED'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const SAVING_USER_DATA = 'SAVING_USER_DATA'
export const SAVING_USER_DATA_SUCCESS = 'SAVING_USER_DATA_SUCCESS'
export const SAVING_USER_DATA_ERROR = 'SAVING_USER_DATA_ERROR'

/**
 * saving data
 * dispatched on start of changePicture action
 * sets saving to true
 */
export function savingUserData(){
    return {
        type: SAVING_USER_DATA
    }
}

export function savingUserDataSuccess(data){
    return {
        type: SAVING_USER_DATA_SUCCESS,
        data
    }
}

export function savingUserDataError(error){
    return {
        type: SAVING_USER_DATA_ERROR,
        error
    }
}




/**
 * chanege user avatar
 * dispatched on avatar picker save button click
 * @param {string} string 
 */
export function changePicture(string){
    return function(dispatch){
        dispatch(savingUserData())

        userChangePicture().then(res => {
            dispatch(savingUserDataSuccess({
                avatar: string,
                profilePhoto: "https://api.adorable.io/avatars/face/"+string,
            }))
        }).catch(err=>{
            dispatch(savingUserDataError(err.message))
        })
    }
}






/**
 * dipatched on login started
 */
export function startLogin(){
    return {
        type: START_LOGIN
    }
}

/**
 * dispatched on login failed
 * @param {Error} error 
 */
export function loginFailed(error){
    return {
        type: LOGIN_FAILED,
        error
    }
}

/**
 * dispatched on login successfull
 * @param {Object} user 
 */
export function loginSuccess(user){
    return {
        type: LOGIN_SUCCESS,
        user
    }
}


 /**
 * dispatched on logout button click
 */
export function logOut(){
    userLogout();
    return {
        type: LOGOUT,
    }
}






/**
 * dispatched on "Log in" button click
 * @param {string} email 
 * @param {string} password 
 */
export function login(email, password){
    return function(dispatch){
        dispatch(startLogin()) // start login spinner

        userLogin(email, password).then(data => {           // login successfull
            dispatch(loginSuccess(data))
        }).catch(error => {                                 // login failed
            dispatch(loginFailed(error))
        }
            
        )
    }
}




