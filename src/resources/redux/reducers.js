import { START_LOGIN, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT, SAVING_USER_DATA, SAVING_USER_DATA_SUCCESS, SAVING_USER_DATA_ERROR, login} from "./actions";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const initialUser = {
    id: "",
    username: "",
    title: "",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    birthday: "",
    school: "",
    profilePhoto: "",
    avatar: "",
    error: "",
    savingData: false,
    dataSaved: false,
}

const initialState = {
    login: {
        logged: false,
        working: false,
        error: "",
    },
    user: initialUser,
}


const initialLogin =  {
    logged: false,
    working: false,
    error: "",
}

var loginReducer = function(login = initialLogin, action){
    switch(action.type){
        case START_LOGIN:
            return Object.assign({}, login, {
                    logged: false,
                    working: true,
                    error: "",
            })
        case LOGIN_FAILED:
            return Object.assign({}, login, {
                    logged: false,
                    working: false,
                    error: action.error.message,
            })
        case LOGIN_SUCCESS:
            return Object.assign({}, login, {
                    logged: true,
                    working: false,
                    error: "",
            })
        case LOGOUT:
            return Object.assign({}, login, {
                    logged: false,
                    working: false,
                    error: "",
        })
        default: 
            return login
    }
}
//loginReducer = persistReducer({key:'login', storage: storage, blacklist:["working","error"]},loginReducer)


var userReducer = function(user = initialUser, action){
    switch(action.type){
        case LOGIN_SUCCESS:
            return {...user,
                ...action.user
            }
        case LOGOUT:
            return initialUser
        case SAVING_USER_DATA:
            return {...user, 
                savingData: true,
                dataSaved: false,
            }
        case SAVING_USER_DATA_ERROR:
            return {...user, 
                savingData: false,
                error: action.error,
                dataSaved: false
            }
        case SAVING_USER_DATA_SUCCESS:
            return {...user, 
                ...action.data,
                savingData: false,
                dataSaved: true,
                error: "",
            }
        default:
            return user
    }

}
//userReducer = persistReducer({key:'user', storage: storage, blacklist:["savingData","error", "dataSaved"]},userReducer)



export function mainReducer(state = initialState, action){
    return {
        login: loginReducer(state.login, action),
        user: userReducer(state.user, action)
    }
}