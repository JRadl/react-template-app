import {createStore, applyMiddleware, compose} from 'redux';
import {mainReducer} from './reducers';
import thunkMiddleware from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //redux dev tools

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['login', 'user']
}

//const persistedReducer = persistReducer(persistConfig, mainReducer)

export const store = createStore(mainReducer, composeEnhancers(
    applyMiddleware(thunkMiddleware)     //async actions middleware
))

//export const persistor = persistStore(store)
